###Восстанавливаем права на доступ и/или группу с пользователем у всей системы(у "испорченых" файлов)
(Данные действия делаем под рутом)

Сначала надо узнать какие файлы не совпадают с удалёнными:  
* `find-broken-chmod.sh` или `sh find-broken-chmod.sh`  
(Пишем это в файл или ведём через пайплайн в следующий скрипт)

Далее получаем список пакетов с "испорчеными" файлами:  
* `find-packages-by-files.sh`    
* `find-broken-chmod.sh | find-packages-by-files.sh` (если с предыдущей командой)  
* `cat list.txt | find-packages-by-files.sh` (если читаем из файла)  
* `echo "/some/path" | find-packages-by-files.sh` (если хотим узнать только для одного пути/файла)

Если знаем, что некоторых юхеров/групп не хватает(опционально):  
* `rpm --setugids`

И, наконец, восстанавливаем заводские привелегии:  
* `mass-fix-chmod.sh`
* `cat file | mass-fix-chmod.sh`
* `echo "poket" | mass-fix-chmod.sh`

####Если надо быстро
`rpm -Va | awk '$2~/^\/(etc|usr|opt|srv|var)/{print $2}' | xargs rpm -qf | sort -u | while read line; do rpm --setperms $line; done;`