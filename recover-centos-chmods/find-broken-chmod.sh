#!/usr/bin/env bash
rpm -Va | awk '$2~/^\/(etc|usr|opt|srv|var)/{print $2}'