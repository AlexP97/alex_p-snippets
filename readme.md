##Alex_P linux snippets
Данный проект представляет собой аналог блога программиста,
где он описывает всякие полезные вещи для администрирования.
Здесь собраны, написанные мной лично или почти нагло стыренные,
скрипты для *nix систем (мб и ещё что) явно упрощающие жизнь жизнь
мне и, возможно, если кто найдёт данную репу, другим людам.

####На данный момент уже есть:
* Восстановление прав/ползователей/групп для CentOS(7) \[recover-centos-chmods\]